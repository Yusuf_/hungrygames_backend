package com.infosupport.hungry_games;

import com.infosupport.hungry_games.controller.ArenaController;
import com.infosupport.hungry_games.factory.ContestantFactory;
import com.infosupport.hungry_games.model.CareerContestant;
import com.infosupport.hungry_games.model.Contestant;
import com.infosupport.hungry_games.model.DistrictContestant;
import com.infosupport.hungry_games.model.Gender;
import com.infosupport.hungry_games.websocket.WebSocketSender;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class HungryGamesApplicationTests {
    private ArenaController arenaController;

    @Before
    public void setup(){
        WebSocketSender webSocketSender = mock(WebSocketSender.class);
        arenaController = new ArenaController(webSocketSender);
    }

    @Test
    public void createDistrictContestant(){
        Contestant contestant = ContestantFactory.getContestant("district");
        System.out.println(contestant);
        Assert.assertTrue(contestant instanceof DistrictContestant);
    }

    @Test
    public void createCareerContestant(){
        Contestant contestant = ContestantFactory.getContestant("career");
        System.out.println(contestant);
        Assert.assertTrue(contestant instanceof CareerContestant);
    }

    @Test
    public void careerContestantGetBattleItemByCreation(){
        List<Contestant> contestants = arenaController.createContestants(1, "career", Gender.MALE);
        Contestant contestant = contestants.get(0);
        int initialHealth = 100;
        int initialAttack = 150;
        Assert.assertTrue(contestant.getHealth() > initialHealth || contestant.getAttack() > initialAttack);
    }

    @Test
    public void createTwelveMaleCContestants(){
        List<Contestant> contestants = arenaController.createContestants(12, "career", Gender.MALE);
        int expectedSize = 12;
        int actualSize = contestants.size();
        Assert.assertEquals(expectedSize, actualSize);
        Contestant contestant = contestants.get(0);
        Assert.assertEquals(contestant.getGender(), Gender.MALE);
    }

}
