package com.infosupport.hungry_games;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HungryGamesApplication {

    public static void main(String[] args) {
        SpringApplication.run(HungryGamesApplication.class, args);
    }
}
