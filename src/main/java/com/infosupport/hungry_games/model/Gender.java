package com.infosupport.hungry_games.model;

public enum Gender {
    MALE, FEMALE
}
