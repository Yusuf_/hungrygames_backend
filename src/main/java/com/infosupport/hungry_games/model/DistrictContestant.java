package com.infosupport.hungry_games.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class DistrictContestant extends Contestant {
    public DistrictContestant(){
        this.setDefense(150);
    }
}
