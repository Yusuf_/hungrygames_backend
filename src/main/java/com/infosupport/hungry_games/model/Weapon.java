package com.infosupport.hungry_games.model;

public class Weapon implements BattleItem {

    public void use(Contestant contestant) {
        int attack = contestant.getAttack() + 20;
        System.out.println("Received attack battle item");

        System.out.println("[" + contestant.getId() + "] gets 20 points attack bonus");
        System.out.println("[" + contestant.getId() + "] Attack " + attack);
        contestant.setAttack(attack);
    }
}
