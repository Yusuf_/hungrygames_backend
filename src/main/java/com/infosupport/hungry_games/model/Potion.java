package com.infosupport.hungry_games.model;

public class Potion implements BattleItem {

    public void use(Contestant contestant) {
        int health = contestant.getHealth() + 20;
        System.out.println("Received health battle item");
        System.out.println("[" + contestant.getId() + "] gets 20 points health bonus");
        System.out.println("[" + contestant.getId() + "] health " + health);

        contestant.setHealth(health);
    }
}
