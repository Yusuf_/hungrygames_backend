package com.infosupport.hungry_games.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import javax.persistence.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CareerContestant.class, name = "CareerContestant"),

        @JsonSubTypes.Type(value = DistrictContestant.class, name = "DistrictContestant") }
)
public abstract class Contestant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int attack;
    private int defense;
    private double chance;
    private int health;
    @Enumerated(EnumType.STRING)
    private Gender gender;

    public Contestant() {
        this.attack = 100;
        this.defense = 100;
        this.health = 100;
    }
}
