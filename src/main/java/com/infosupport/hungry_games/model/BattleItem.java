package com.infosupport.hungry_games.model;

public interface BattleItem {
    void use(Contestant contestant);
}
