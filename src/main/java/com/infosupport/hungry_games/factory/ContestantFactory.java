package com.infosupport.hungry_games.factory;

import com.infosupport.hungry_games.model.CareerContestant;
import com.infosupport.hungry_games.model.Contestant;
import com.infosupport.hungry_games.model.DistrictContestant;


public class ContestantFactory {

    public static Contestant getContestant(String type){
        if(type.equals("career")){
            return new CareerContestant();
        }
        else if(type.equals("district")){
            return new DistrictContestant();
        }
        return null;
    }
}
