package com.infosupport.hungry_games.controller;

import com.infosupport.hungry_games.model.Contestant;
import com.infosupport.hungry_games.websocket.WebSocketSender;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
public class HungryGameController {
    private final WebSocketSender webSocketSender;
    private ArenaController arenaController;

    @Autowired
    public HungryGameController(WebSocketSender webSocketSender) {
        this.webSocketSender = webSocketSender;
    }

    @RequestMapping(
            path = "/start",
            method = RequestMethod.POST
    )
    public void startSimulation(){
        arenaController = new ArenaController(webSocketSender);
        arenaController.setup();
    }

    @RequestMapping(
            path = "/battle",
            method = RequestMethod.POST
    )
    public void simulateBattleDay(){
        arenaController.battleDay();
    }

    @MessageMapping("/contestants")
    public Contestant getContestants(Contestant contestant){
        return contestant;
    }

    @MessageMapping("/battleItems")
    public JSONObject getBattleItems(JSONObject jsonObject){
        return jsonObject;
    }

    @MessageMapping("/battlelog")
    public JSONObject getBattleLog(JSONObject jsonObject){
        return jsonObject;
    }

    @MessageMapping("/death")
    public Contestant getDeadContestant(Contestant contestant){
        return contestant;
    }

    @MessageMapping("/winner")
    public Contestant getWinningContestant(Contestant contestant){
        return contestant;
    }

    @RequestMapping(
            path = "/favor",
            method = RequestMethod.POST
    )
    public void favorBattleItemContestant(@RequestBody Contestant contestant){
        arenaController.findRandomBattleItem(contestant);
    }
}
