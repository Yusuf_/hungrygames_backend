package com.infosupport.hungry_games.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.infosupport.hungry_games.factory.ContestantFactory;
import com.infosupport.hungry_games.model.*;
import com.infosupport.hungry_games.websocket.WebSocketSender;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Controller
public class ArenaController {
    private List<Contestant> contestants;
    private final double BATTLE_CHANCE = 0.1;
    private final double LOOT_CHANCE = 0.2;
    private final int BOOST_LIMIT = 50;
    private Random random;
    private int id;
    private ObjectMapper mapper = new ObjectMapper();
    private WebSocketSender webSocketSender;

    public ArenaController(WebSocketSender webSocketSender) {
        contestants = new ArrayList<>();
        this.webSocketSender = webSocketSender;
        random = new Random();
    }

    /**
     * Returns a list of contestants that will fight in the hungry games. You need to give the amount, type of
     * contestants and gender.
     *
     * @param amount the amount of contestants that will be created.
     * @param type   the type of contestant. This can be "career" of "district"
     * @param gender the gender of the contestant. This is an enum that cen be "female" or "male". More genders will
     *               be added in the future.
     * @return list of contestants.
     */
    public List<Contestant> createContestants(int amount, String type, Gender gender) {
        List<Contestant> contestants = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Contestant contestant = ContestantFactory.getContestant(type);
            contestant.setId(id);
            contestant.setGender(gender);

            if (contestant instanceof CareerContestant)
                findRandomBattleItem(contestant);

            int health = contestant.getHealth() + (random.nextInt(BOOST_LIMIT));
            contestant.setHealth(health);

            int attack = contestant.getAttack() + (random.nextInt(BOOST_LIMIT));
            contestant.setAttack(attack);

            int defense = contestant.getDefense() + (random.nextInt(BOOST_LIMIT));
            contestant.setDefense(defense);

            double chance = random.nextDouble();
            contestant.setChance(chance);

            contestants.add(contestant);
            webSocketSender.sendByWebSocket("/topic/contestants", contestant, 300);
            id++;

        }
        return contestants;
    }

    /**
     * This will battleDay the infamous Hungry games. The contestants are created and added to the list of participating
     * contestants.
     */
    public void setup() {
        List<Contestant> careerMaleContestants = createContestants(3, "career", Gender.MALE);
        List<Contestant> districtMaleContestants = createContestants(3, "district", Gender.MALE);
        List<Contestant> districtFemaleContestants = createContestants(6, "district", Gender.FEMALE);

        contestants.addAll(careerMaleContestants);
        contestants.addAll(districtMaleContestants);
        contestants.addAll(districtFemaleContestants);
    }

    /**
     * This will simulate a battle to the death between two contestants. It will randomly select two contestants who
     * fight. The remaining contestant will be killed by the President.
     */
    public void battleDay() {
        if (contestants.size() > 1) {
            System.out.println("Current remaining contestants " + contestants.size());
            if (random.nextDouble() > BATTLE_CHANCE) {
                Collections.shuffle(contestants);
                battle(contestants.remove(0), contestants.remove(0));
            } else {
                System.out.println("No battle this round");
            }
        } else {
            System.out.println("**************************");
            System.out.println("Remaining " + contestants);
            Contestant winner = contestants.get(0);
            System.out.println("[President Snow] >> BRUTALITY >> [" + winner.getId() + "]");
            System.out.println("**************************");
        }
    }

    /**
     * Two contestants will fight to death.
     *
     * @param contestantOne the attacking contestant
     * @param contestantTwo the defending contestant
     */
    private void battle(Contestant contestantOne, Contestant contestantTwo) {
        int initialHealthOne = contestantOne.getHealth();
        int initialHealthTwo = contestantTwo.getHealth();

        do {
            fight(contestantOne, contestantTwo);
        } while (contestantOne.getHealth() > 0 && contestantTwo.getHealth() > 0);

        if (contestantOne.getHealth() > 0) {
            System.out.println("[" + contestantOne.getId() + "] wins!");
            endMatch(contestantOne, initialHealthOne);
            webSocketSender.sendByWebSocket("/topic/death", contestantTwo, 500);
            webSocketSender.sendByWebSocket("/topic/winner", contestantOne, 500);

        } else if (contestantTwo.getHealth() > 0) {
            System.out.println("[" + contestantTwo.getId() + "] wins!");
            endMatch(contestantTwo, initialHealthTwo);
            webSocketSender.sendByWebSocket("/topic/death", contestantOne, 500);
            webSocketSender.sendByWebSocket("/topic/winner", contestantTwo, 500);
        }
        System.out.println("=============================");
    }

    /**
     * The winning contestant's health is reset to full. If the contestant is lucky he will get a battle item that will
     * boost his health or attack. The winner gets added back to list of remaining contestants
     *
     * @param contestant winning contestant
     */
    private void endMatch(Contestant contestant, int health) {
        contestant.setHealth(health);
        if (random.nextDouble() > LOOT_CHANCE)
            findRandomBattleItem(contestant);

        contestants.add(contestant);
    }

    /**
     * Create random battle item and assign to a contestant. The battle item can be a potion or weapon.
     *
     * @param contestant the contestant that maybe will receive a battle item
     */
    public void findRandomBattleItem(Contestant contestant) {
        BattleItem battleItem;
        JSONObject jsonObject = new JSONObject();

        int boost = 20;
        if (random.nextBoolean()) {
            battleItem = new Potion();
            jsonObject.put("type", "Potion");
        } else {
            battleItem = new Weapon();
            jsonObject.put("type", "Weapon");
        }
        battleItem.use(contestant);
        jsonObject.put("boost", boost);
        try {
            jsonObject.put("contestant", mapper.writeValueAsString(contestant));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        webSocketSender.sendByWebSocket("/topic/battleItems", jsonObject.toString(), 500);
    }

    /**
     * Fight between two contestants till their health is above 0.
     *
     * @param contestantOne attacking contestant that will deal damage.
     * @param contestantTwo defending contestant that will receive damage.
     */
    private void fight(Contestant contestantOne, Contestant contestantTwo) {
        attack(contestantTwo, contestantOne);
        attack(contestantOne, contestantTwo);
    }

    private void attack(Contestant attacker, Contestant defender) {
        JSONObject jsonObject = new JSONObject();
        if (defender.getHealth() > 0 && attacker.getHealth() > 0) {

            int damage = strike(attacker, defender);
            try {
                jsonObject.put("attacker", mapper.writeValueAsString(attacker));
                jsonObject.put("defender", mapper.writeValueAsString(defender));
                jsonObject.put("damage", damage);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            webSocketSender.sendByWebSocket("/topic/battlelog", jsonObject.toString(), 500);
        }
    }

    private int strike(Contestant attacker, Contestant defender) {
        int possibleDamage = attacker.getAttack() - defender.getDefense();
        int damage = possibleDamage <= 0 ? random.nextInt(BOOST_LIMIT) : possibleDamage;
        int health = (defender.getHealth() - damage) < 0 ? 0 : defender.getHealth() - damage;

        System.out.println("[" + attacker.getId() + "] >> " + damage + " >> [" + defender.getId() + "]");
        System.out.println("[" + defender.getId() + "] Health: " + health);

        defender.setHealth(health);
        return damage;
    }
}
