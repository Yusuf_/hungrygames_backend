package com.infosupport.hungry_games.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketSender {
    @Autowired
    private SimpMessagingTemplate template;

    public void sendByWebSocket(String path, Object object, long timeout){
        template.convertAndSend(path, object);
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
